//
//  RegisterPageViewController.swift
//  RentRocketApp2.0
//
//  Created by Nunez, Claude Joseph on 1/31/18.
//  Copyright © 2018 Claude Nunez. All rights reserved.
//

import UIKit

class RegisterPageViewController: UIViewController {

    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func registerButtonTapped(_ sender: Any) {
        
        let userEmail = userEmailTextField.text;
        let userPassword = userPasswordTextField.text;
        let userRepeatPassword = repeatPasswordTextField.text;
        
        // Check for empty fields
        if(userEmail?.isEmpty || userPassword.isEmpty || userRepeatPassword.isEmpty)
        
        {
            
            //Display alert message
            displayMyAlertMessage("All Fields are required");
            
            return;
        }
        //Check if passwords match
        if(userPassword != userRepeatPassword)
        
        {
            //Display an alert message
            displayMyAlertMessage("Passwords do not match");
            
            
            return;
            
        }
        
        // Store Data
        UserDefaults.standardUserDefaults().setObject(userEmail, forKey: "userEmail");
        
        UserDefaults.standardUserDefaults().setObject(userEmail, forKey: "userPassword");
        
        UserDefaults.standardUserDefaults().synchronize();
        
        
        
        //Display alert message with confirmation
        
        
        
        //I left off in this area way more tomorrow but not too bad. 
        var myAlert = UIAlertController(title:"Alert", message:"Registration is successful. Thank you!"
            userMessage, preferredStyle:
            UIAlertControllerStyle.alert);
        
        
        
    }
    
    
    
    
    func displayMyAlertMessage(userMessage:String)
    {
        var myAlert = UIAlertController(title:"Alert", message:
            userMessage, preferredStyle:
            UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler:nil);
        
        myAlert.addAction(okAction);
        
        self.presentViewController(myAlert, animated:true, completion:nil);
        
    }

   

}
