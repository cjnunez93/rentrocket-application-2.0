//
//  ViewController.swift
//  RentRocketApp2.0
//
//  Created by Claude Nunez on 1/30/18.
//  Copyright © 2018 Claude Nunez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.shouldPerformSegue(withIdentifier: "loginView", sender: self)
        
    }

}

